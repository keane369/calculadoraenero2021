import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Calculadora {

    @Test
    public void testCalculadora() throws MalformedURLException, InterruptedException {

        //Desired Capabilities
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "11");
        capabilities.setCapability("appActivity", "com.android.calculator2.Calculator");
        capabilities.setCapability("appPackage", "com.google.android.calculator");

        //Inicializar el driver
        AndroidDriver<AndroidElement> driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"),capabilities);

        /*
        //Metodos
        //Click - findElement (1)
        System.out.println("Trying to click on 9 button");
        WebElement nueve = driver.findElement(By.id("digit_9"));
        nueve.click();
        System.out.println("9 button was clicked");

        //Click - findElements (1 o mas)
        System.out.println("Trying to click on 8 button");
        List<AndroidElement> ocho = driver.findElements(By.id("digit_8"));
        System.out.println("Tamaño de la lista: "+ ocho.size());
        ocho.get(0).click();
        System.out.println("8 button was clicked");

        //Send Keys
        System.out.println("Trying to send keys to result element");
        WebElement result = driver.findElement(By.id("formula"));
        result.sendKeys("9");
        System.out.println("Text was sent");

        //Get Text
        System.out.println("Resultado es: "+ result.getText());

        //Time out
        Thread.sleep(2000);

        //Cerrar App
        driver.closeApp();

        */
        //Ejercicio
        WebElement nueve = driver.findElement(By.id("digit_9"));
        WebElement tres = driver.findElement(By.id("digit_3"));

        WebElement resta = driver.findElement(By.id("op_sub"));
        WebElement mult = driver.findElement(By.id("op_mul"));
        WebElement suma = driver.findElement(By.id("op_add"));
        WebElement igual = driver.findElement(By.id("eq"));
        List<Double> datos = new ArrayList<Double>();
        Double resultadoFinal = 0.0;

        //Realizar 3 operaciones y Guardar resultados en una lista
        //Suma
        nueve.click();
        suma.click();
        tres.click();
        igual.click();
        WebElement result = driver.findElement(By.id("result_final"));
        System.out.println("Resultado 1: "+ result.getText());
        datos.add(Double.parseDouble(result.getText()));

        //Resta
        resta.click();
        tres.click();
        igual.click();
        System.out.println("Resultado 2: "+ result.getText());
        datos.add(Double.parseDouble(result.getText()));

        //Multiplicacion
        mult.click();
        nueve.click();
        igual.click();
        System.out.println("Resultado 3: "+ result.getText());
        datos.add(Double.parseDouble(result.getText()));

        //Sumar los resultados
        for (Double valor:datos){
            resultadoFinal += valor;
        }

        for(int i = 0; i < datos.size(); i++){
            resultadoFinal = resultadoFinal + datos.get(i);
        }

        /*IF/Else Mandar a imprimir un mensaje
                - Resultado < 100, print “Numero inferior a 100”
                - Resultado 100 -  200, print “Numero mayor a 100”
                - Resultado 200 -  300, print “Numero mayor a 200”
                - Else… “Cantidad no registrado”
        */

        System.out.println("Valor final es: "+resultadoFinal);
        if(resultadoFinal < 100){
            System.out.println("Numero inferior a 100");
        }else if (resultadoFinal >= 100 && resultadoFinal < 200)
            System.out.println("Numero mayor a 100");
        else if (resultadoFinal >= 200 && resultadoFinal < 300)
            System.out.println("Numero mayor a 200");
        else
            System.out.println("Cantidad no registrada");

        driver.closeApp();
    }
}
